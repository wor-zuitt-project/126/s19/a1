//Arrow Function 
// ES 6 ECMAScript 6

function sum(a,b){
	return a+b
}

//Step 1
//let sum2


//Step 2
//let sum2 = () =>

//Step 3
/*let sum2 = (a,b) => {
	return a+b
}
*/


//Step 4
let sum2 = (a,b) => a+b

/*Note 
function sum(a,b){
	return a+b
} 
Here it's called Function Declaration 




let x = 9;
This is an expression



let sum2 = (a,b) => a+b
Even if we use let, it's still function because
1. It's a varaible that contain a function
2. So it's call 'Function Expression'
3. It's like sum2 is the name of a function


Reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
	
สูตรคือ พาราเมเตอ ชี้ไปที่ ค่ารีเทิร์น
แต่ถ้าฟังก์ชันมีชื่อด้วย ให้เติม let 'name' = เข้าไปข้างหน้า


a => a >=0 เราเขียนแบบนี้ได้เลย แต่ถ้ามีมากกว่าหนึ่งพาราเมเตอร์ ต้องใส่ ()
ถ้ามีมากกว่า1statement ก็ใส่ {} ไปเหมือนเดิม เช่น 
let giveAnswer = (num1, num2) => {
	let total = num1 + num2
	return total
}

ดังนั้น General Form คือ

let ... = () => {
	
}
*/






function isPositive(a){
	return a >= 0
}

let isPositive2 = a => a >=0


function someNumber(){
	return Math.random()
}


let someNumber2 = () => Math.random()


let hello = () => "Hello World!"

let square = a => a*a;

let giveAnswer = (num1, num2) => {
	let total = num1 + num2
	return total
}



//Object Destructuring 

const address = {
	street: 'Times St.',
	city: 'New York',
	country: 'USA'
}


const {city, street, country} = address
/* This is the same as 

const street = address.street;
const city = address.city;
const country = address.country;
*/


//Array Map Method 
const colors = ['red', 'green','blue']
// const items = colors.map(function(color){
// 	return "<li>" + color + "</li>";
// })

const items = colors.map(color => `<li>${color}</li>`)
